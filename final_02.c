#include <stdio.h>
#include <string.h>

int main()
{
  char palabra1[50], palabra2[50];
  int cont1, cont2;

  printf("Ingrese palabra 1: ");
  gets(palabra1);

  printf("Ingrese palabra 2: ");
  gets(palabra2);

  cont1 = strlen(palabra1);
  cont2 = strlen(palabra2);

  printf("Longitud palabra 1: %d\n", cont1);
  printf("Longitud palabra 2: %d\n", cont2);

  if (strcmp(palabra1, palabra2) < 0)
    printf("%s %s\n", palabra1, palabra2);
  else
    printf("%s %s\n", palabra2, palabra1);

  return 0;
}